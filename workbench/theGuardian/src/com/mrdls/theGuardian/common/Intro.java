package com.mrdls.theGuardian.common;

import java.awt.Graphics;

public class Intro {

	public static int frame = 0;
	public static int introLength = 180;
	
	public static String title = Guardian.name + " " + Guardian.version;
	
	public static void render(Graphics g){
		if(frame < introLength){
			g.setFont(Guardian.fontTitle);
			Guardian.fontMetrics = g.getFontMetrics();
			int titleWidth = Guardian.fontMetrics.stringWidth(title);
			g.drawString(title, Guardian.screenSize.width / 2 - titleWidth / 2, 200);
			
			g.setFont(Guardian.fontSubtitle);
			Guardian.fontMetrics = g.getFontMetrics();
			int companyWidth = Guardian.fontMetrics.stringWidth(Guardian.companyName);
			g.drawString(Guardian.companyName, Guardian.screenSize.width / 2 - companyWidth / 2, 300);
			frame++;
		}else{
			Guardian.screenState = 1;
		}
	}
}
